/* eslint-disable no-undef */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testTimeout: 120000,
  reporters: [
    "default",
    ["jest-junit",
      {
        suiteName: "jest tests",
        outputDirectory: "./out",
        outputName: "junit.xml",
      }]
  ]
}