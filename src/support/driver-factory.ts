/* eslint-disable @typescript-eslint/no-explicit-any */
import { Builder } from "selenium-webdriver";

export class DriverFactory {

  static async getBrowser(capabilities: any) {
    const driver = await new Builder()
      .forBrowser('chrome')
      .usingServer(`http://localhost:4444/wd/hub`)
      .withCapabilities(capabilities)
      .build();

    return driver;
  }
}


